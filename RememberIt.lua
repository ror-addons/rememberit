--[[
RememberIt
by SilverWF
Idea from 'RememberWhen' addon for WoW
Based on 'Phantom'
]]
--------
RememberIt = {}
local screencounter = 4
local makescreen = false
local pairs = pairs
local ipairs = ipairs
local tonumber = tonumber
local strmatch = wstring.match
local strlen = wstring.len
local RIDEFset = {
   onSc = true,
   onDeath = true,
   onKill = true,
   onRRup = true,
   onRup = true,
   deathlist = L"",
   killlist = L""
   }

local MyRealm = GameData.Player.realm -- 1 for Order and 2 for Destro
local MyRRank = GameData.Player.Renown.curRank
local MyRank = GameData.Player.level
local MyKillChannel = nil
local MyDeathChannel = nil
local OldRank = GameData.Player.level
local OldRRank = GameData.Player.Renown.curRank

if MyRealm == 1 then -- setup default channels to parse your kills and deaths
   MyKillChannel = SystemData.ChatLogFilters.RVR_KILLS_ORDER
   MyDeathChannel = SystemData.ChatLogFilters.RVR_KILLS_DESTRUCTION
   elseif MyRealm == 2 then
   MyKillChannel = SystemData.ChatLogFilters.RVR_KILLS_DESTRUCTION
   MyDeathChannel = SystemData.ChatLogFilters.RVR_KILLS_ORDER
end

if not RememberIt.Settings then
    RememberIt.Settings = RIDEFset
else
    for k,v in pairs(RIDEFset) do
        if not RememberIt.Settings[k] then RememberIt.Settings[k] = v end
    end
end -- end of function

function RememberIt.CloseSettingsWindow()
    WindowSetShowing("RememberItSettings", false)
end -- end of function

function RememberIt.SaveSettings()

    RememberIt.Settings.onSc = ButtonGetPressedFlag("RememberItSettingsOnScenarioEnd")
    RememberIt.Settings.onDeath = ButtonGetPressedFlag("RememberItSettingsOnDeath")
    RememberIt.Settings.onKill = ButtonGetPressedFlag("RememberItSettingsOnKill")
    RememberIt.Settings.onRRup = ButtonGetPressedFlag("RememberItSettingsOnRenownRankUp")
    RememberIt.Settings.onRup = ButtonGetPressedFlag("RememberItSettingsOnRankUp")
    RememberIt.Settings.deathlist = TextEditBoxGetText("RememberItSettingsDeathList")
    RememberIt.Settings.killlist = TextEditBoxGetText("RememberItSettingsKillList")    

end -- end of function

function RememberIt.PopulateWindow()
    ButtonSetPressedFlag("RememberItSettingsOnScenarioEnd", RememberIt.Settings.onSc)
    ButtonSetPressedFlag("RememberItSettingsOnDeath", RememberIt.Settings.onDeath)
    ButtonSetPressedFlag("RememberItSettingsOnKill", RememberIt.Settings.onKill)
    ButtonSetPressedFlag("RememberItSettingsOnRenownRankUp", RememberIt.Settings.onRRup)
    ButtonSetPressedFlag("RememberItSettingsOnRankUp", RememberIt.Settings.onRup)
    TextEditBoxSetText("RememberItSettingsDeathList", RememberIt.Settings.deathlist)
    TextEditBoxSetText("RememberItSettingsKillList", RememberIt.Settings.killlist)
end -- end of function

function RememberIt.Show()
    WindowSetShowing("RememberItSettings", true)
end -- end of function

function RememberIt.OnLoad()    
    if LibSlash and not LibSlash.IsSlashCmdRegistered("reit") then
        LibSlash.RegisterSlashCmd("reit", RememberIt.Show)
    end
end -- end of function

-- Initialisation of addon. Register events.
function RememberIt.Initialize ()
if SystemData.Settings.Language.active ~= 1 then 
   if SystemData.Settings.Language.active == 2 then
      EA_ChatWindow.Print(L"'RememberIt' fonctionne uniquement avec la localisation en anglais. Pour l'installer, tapez le texte '/lang 1'.")
      elseif SystemData.Settings.Language.active == 3 then
      EA_ChatWindow.Print(L"'RememberIt' funktioniert nur mit der englischen Lokalisierung. Um es zu installieren, geben Sie den Text '/lang 1'.")
      elseif SystemData.Settings.Language.active == 10 then
      EA_ChatWindow.Print(L"'RememberIt' работает только на английской локализации. Чтобы установить ее введите '/lang 1'.")
      else                                                                                                          
      EA_ChatWindow.Print(L"'RememberIt' will working only at English localisation. To set up it, please enter text '/lang 1'.")
   end
return 
end

    EA_ChatWindow.Print(L"'RememberIt' initialized. To open settings enter '/reit'")
    CreateWindow("RememberItSettings", false)
    LabelSetText("RememberItSettingsTitleBarText", L"RememberIt settings")

    LabelSetText("RememberItSettingsOnScenarioEndLabel", L"On scenario ends")
    ButtonSetCheckButtonFlag("RememberItSettingsOnScenarioEnd", true)
    
    LabelSetText("RememberItSettingsOnRenownRankUpLabel", L"On renown rank up")
    ButtonSetCheckButtonFlag("RememberItSettingsOnRenownRankUp", true)
    
    LabelSetText("RememberItSettingsOnRankUpLabel", L"On rank up")
    ButtonSetCheckButtonFlag("RememberItSettingsOnRankUp", true)

    LabelSetText("RememberItSettingsOnDeathLabel", L"On your every death or from list of names below")
    ButtonSetCheckButtonFlag("RememberItSettingsOnDeath", true)

    LabelSetText("RememberItSettingsDeathListLabel", L"")
    TextEditBoxSetText("RememberItSettingsDeathList", RememberIt.Settings.deathlist)
        
    LabelSetText("RememberItSettingsOnKillLabel", L"On your every kill or deathblow to any of names below")
    ButtonSetCheckButtonFlag("RememberItSettingsOnKill", true)

    LabelSetText("RememberItSettingsKillListLabel", L"")
    TextEditBoxSetText("RememberItSettingsKillList", RememberIt.Settings.killlist)

   RegisterEventHandler (TextLogGetUpdateEventId ("Combat"), "RememberIt.MakeScreen")
   RegisterEventHandler (TextLogGetUpdateEventId ("Combat"), "RememberIt.KillParser")
   RegisterEventHandler (TextLogGetUpdateEventId ("Combat"), "RememberIt.ExpParser")
   RegisterEventHandler(SystemData.Events.LOADING_END, "RememberIt.OnLoad")
   RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "RememberIt.OnLoad")
   RegisterEventHandler(SystemData.Events.CITY_SCENARIO_END , "RememberIt.onScenarionEnd")
   RegisterEventHandler(SystemData.Events.SCENARIO_END , "RememberIt.onScenarionEnd")
--   RegisterEventHandler(SystemData.Events.KILLER_NAME_UPDATED , "RememberIt.onDeath") 
--   RegisterEventHandler(SystemData.Events.PLAYER_RVR_STATS_UPDATED , "RememberIt.onKill") 
--   RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_RANK_UPDATED , "RememberIt.onRRankUp") 
--   RegisterEventHandler(SystemData.Events.PLAYER_CAREER_RANK_UPDATED , "RememberIt.onRankUp")   
end -- end of function


function RememberIt.onScenarionEnd () -- Take screenshot on scenario end
if RememberIt.Settings.onSc then
   RememberIt.MakeScreen()
   return
   else return
end
end -- end of function


function RememberIt.KillParser (updateType, filterType) -- 
if not RememberIt.Settings.onDeath and not RememberIt.Settings.onKill then return end
if updateType ~= SystemData.TextLogUpdate.ADDED or (filterType ~= MyKillChannel and filterType ~= MyDeathChannel) then return end

local time, t, msg = TextLogGetEntry ("Combat", TextLogGetNumEntries ("Combat") - 1) -- Get newest message here
if not (msg) then return end  -- if no message then stop 

local myname = wstring.sub( GameData.Player.name,1,-3 )
local victim, killer = strmatch(msg, L"(%a+) has been %a+ by (%a+)%'s.+")

if victim == myname and RememberIt.Settings.onDeath then
      local killername = towstring(string.upper(tostring(killer)))
      local dlist = towstring(string.upper(tostring(RememberIt.Settings.deathlist)))
      if dlist == L"" or (strmatch(dlist, killername) ~= nil) then
         if killername ~= myname then EA_ChatWindow.Print(L"RememberIt: I was killed by "..killername..L"!") else EA_ChatWindow.Print(L"RememberIt: I was killed by myself! ;-(") end
         makescreen = true
         return
      else return
      end     
elseif killer == myname and RememberIt.Settings.onKill then
      local murdered = towstring(string.upper(tostring(victim)))
      local klist = towstring(string.upper(tostring(RememberIt.Settings.killlist)))
      if klist == L"" or (strmatch(dlist, murdered) ~= nil) then
         EA_ChatWindow.Print(L"RememberIt: I have finished off "..murdered..L"!")
         makescreen = true
         return
         else return
      end           
else return
end
end -- end of function


function RememberIt.ExpParser (updateType, filterType) -- 
if updateType ~= SystemData.TextLogUpdate.ADDED or (filterType ~= SystemData.ChatLogFilters.EXP and filterType ~= SystemData.ChatLogFilters.RENOWN) then return end
local time, t, msg = TextLogGetEntry ("Combat", TextLogGetNumEntries ("Combat") - 1) -- Get newest message here
if not (msg) then return end  -- if no message then stop 
local NewRank = GameData.Player.level
local NewRRank = GameData.Player.Renown.curRank

if OldRank < NewRank and RememberIt.Settings.onRup then
   EA_ChatWindow.Print(L"RememberIt: My rank has increased to "..towstring(NewRank)..L"!")
   OldRank = NewRank   
   makescreen = true
   return
end
if OldRRank < NewRRank and RememberIt.Settings.onRRup then
   EA_ChatWindow.Print(L"RememberIt: My renown rank has increased to "..towstring(NewRRank)..L"!")
   OldRRank = NewRRank
   makescreen = true
   return
end
end -- end of function


function RememberIt.MakeScreen () -- Taking screenshot here. This at first see unneeded function maked to give a 1-2 sec pause before taking screenshot or your screenshot will be uninformative
if updateType ~= SystemData.TextLogUpdate.ADDED and not makescreen then return end
local time, t, msg = TextLogGetEntry ("Combat", TextLogGetNumEntries ("Combat") - 1)
if screencounter ~= 0 then
   screencounter = screencounter - 1
   else
   screencounter = 4
   makescreen = false
   TakeScreenShot()
end
end -- end of function