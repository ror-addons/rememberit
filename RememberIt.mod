<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	               
  <UiMod name="RememberIt" version="1.0" date="14/09/2013" >		                         
    <Author name="SilverWF" email="silverwf@gmail.com" />		                         
    <Description text="RememberIt will automatically make a screenshot in some moments: Rank up, Renown Rank up, Scenario end, you have kill or been killed by specific name." />	                                  
    <VersionSettings gameVersion="1.9.9" />      	                                      
    <Dependencies>                                                   
      <Dependency name="LibSlash" />                                     
    </Dependencies>               
    <SavedVariables>             
      <SavedVariable name="RememberIt.Settings" />         
    </SavedVariables>        		                             
    <Files>			                                       
      <File name="RememberIt.lua" />            
      <File name="RememberIt.xml" />		                             
    </Files>		 		                             
    <OnInitialize>                                                   
      <CallFunction name="RememberIt.Initialize" />		                             
    </OnInitialize>	                                 
    <WARInfo>                                           
      <Categories>                                                   
        <Category name="RVR" />	                                                    
        <Category name="OTHER" />                                           
      </Categories>                                           
      <Careers>                                                         
        <Career name="BLACKGUARD" />                                                         
        <Career name="WITCH_ELF" />                                                         
        <Career name="DISCIPLE" />                                                         
        <Career name="SORCERER" />                                                         
        <Career name="IRON_BREAKER" />                                                         
        <Career name="SLAYER" />                                                         
        <Career name="RUNE_PRIEST" />                                                         
        <Career name="ENGINEER" />                                                         
        <Career name="BLACK_ORC" />                                                         
        <Career name="CHOPPA" />                                                         
        <Career name="SHAMAN" />                                                         
        <Career name="SQUIG_HERDER" />                                                         
        <Career name="WITCH_HUNTER" />                                                         
        <Career name="KNIGHT" />                                                         
        <Career name="BRIGHT_WIZARD" />                                                         
        <Career name="WARRIOR_PRIEST" />                                                         
        <Career name="CHOSEN" />                                                         
        <Career name="MARAUDER" />                                                         
        <Career name="ZEALOT" />                                                         
        <Career name="MAGUS" />                                                         
        <Career name="SWORDMASTER" />                                                         
        <Career name="SHADOW_WARRIOR" />                                                         
        <Career name="WHITE_LION" />                                                         
        <Career name="ARCHMAGE" />                                           
      </Careers>                            
    </WARInfo>		 		 	               
  </UiMod>
</ModuleFile>